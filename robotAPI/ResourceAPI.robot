*** Settings ***
Documentation   Documentação da API: https://fakerestapi.azurewebsites.net/index.html
Library         RequestsLibrary
Library         Collections

*** Variables ***
${URL_API}      https://fakerestapi.azurewebsites.net/api/v1/
&{BOOK_ID}      id=89
...             title=Book 89
...             pageCount=8900
&{BOOK_PAGE}    id=89
...             title=Book 89
...             pageCount=8900
&{AUTHORS_ID}   id=257
...             idBook= 89

*** Keywords ***
#SETUP E TEARDOWNS 
Conectar a minha API
    Create Session      fakerAPI    ${URL_API}

#####Ações
Requisitar todos os livros
    ${RESPOSTA}     Get Request     fakerAPI   Books
    log                 ${RESPOSTA.text}
    Set Test Variable   ${RESPOSTA}

Requisitar o livro "${ID_LIVRO}"
    ${RESPOSTA}     Get Request     fakerAPI   Books/${ID_LIVRO}
    log                 ${RESPOSTA.text}
    Set Test Variable   ${RESPOSTA}

Requisitar o numero de paginas do livro "${ID_LIVRO}"
    ${RESPOSTA}     Get Request     fakerAPI   Books/${ID_LIVRO}
    log                 ${RESPOSTA.text}
    Set Test Variable   ${RESPOSTA}


Requisitar o numero de autores do livro "${ID_LIVRO}"
    ${RESPOSTA}     Get Request     fakerAPI    Authors/authors/Books/${ID_LIVRO}
    log                 ${RESPOSTA.text}
    Set Test Variable   ${RESPOSTA}

#####Conferências
Conferir o status code
    [Arguments]                 ${STATUSCODE_DESEJADO}
    Should Be Equal As Strings  ${RESPOSTA.status_code}  ${STATUSCODE_DESEJADO}

Conferir o reason
    [Arguments]                    ${REASON_DESEJADO}
    Should Be Equal As Strings     ${RESPOSTA.reason}        ${REASON_DESEJADO}

Conferir se retorna uma lista com "${QTDE_LIVROS}" livros
    Length Should Be               ${RESPOSTA.json()}        ${QTDE_LIVROS}

Conferir se retorna todos os dados corretos do livro 89
    Dictionary Should Contain Item  ${RESPOSTA.json()}    id         ${BOOK_ID.id}
    Dictionary Should Contain Item  ${RESPOSTA.json()}    title      ${BOOK_ID.title}
    Dictionary Should Contain Item  ${RESPOSTA.json()}    pageCount  ${BOOK_ID.pageCount}      
    Should Not Be Empty             ${RESPOSTA.json()["description"]}
    Should Not Be Empty             ${RESPOSTA.json()["excerpt"]}
    Should Not Be Empty             ${RESPOSTA.json()["publishDate"]}

Conferir se retorna o numero de paginas do livro 89
    Dictionary Should Contain Item  ${RESPOSTA.json()}    id         ${BOOK_PAGE.id}
    Dictionary Should Contain Item  ${RESPOSTA.json()}    title      ${BOOK_PAGE.title}
    Dictionary Should Contain Item  ${RESPOSTA.json()}    pageCount  ${BOOK_PAGE.pageCount}      
    Should Not Be Empty             ${RESPOSTA.json()["description"]}
    Should Not Be Empty             ${RESPOSTA.json()["excerpt"]}
    Should Not Be Empty             ${RESPOSTA.json()["publishDate"]}
Conferir se retorna o numero de autores do livro 89
    Dictionary Should Contain Item  ${RESPOSTA.json()}    id         ${AUTHORS_ID.id}   
    Dictionary Should Contain Item  ${RESPOSTA.json()}    idBook     ${AUTHORS_ID.idBook}
    Should Not Be Empty             ${RESPOSTA.json()["firstName"]}
    Should Not Be Empty             ${RESPOSTA.json()["lastName"]}
    Should Not Be Empty             ${RESPOSTA.json()["id"]}
    Should Not Be Empty             ${RESPOSTA.json()["idBook"]}
    Should Not Be Empty             ${RESPOSTA.json()["firstName"]}
    Should Not Be Empty             ${RESPOSTA.json()["lastName"]}     


