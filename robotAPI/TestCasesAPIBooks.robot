*** Settings ***
Documentation   Documentação da API: https://fakerestapi.azurewebsites.net/index.html
Resource        ../robotAPI/ResourceAPI.robot
Suite Setup     Conectar a minha API


*** Test Cases ***
Buscar a listagem de todos os livros (GET em todos os livros)
    Requisitar todos os livros
    Conferir o status code  200
    Conferir o reason  OK
    Conferir se retorna uma lista com "200" livros
Buscar um livro especifico (GET em um livro especifico)
    Requisitar o livro "89"
    Conferir o status code  200
    Conferir o reason  OK
    Conferir se retorna todos os dados corretos do livro 89

Buscar o numero de paginas no livro especifico (GET em um livro especifico
    Requisitar o numero de paginas do livro "89"
    Conferir o status code  200
    Conferir o reason  OK
    Conferir se retorna o numero de paginas do livro 89
Buscar a listagem de todos os autores de um livro especifico (GET em todos autores do livro especifico)
    Requisitar o numero de autores do livro "89"
    Conferir o status code  200
    Conferir o reason  OK
    Conferir se retorna o numero de autores do livro 89



